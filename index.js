#!/usr/bin/env node
// npm uninstall -g .; npm install -g .; init-gitlab-page
const fs = require('fs')
const { exec } = require('child_process')
const buildYAMLDoc = require('./lib/build-yaml-doc')

exec("git remote -v", (error, stdout, stderr) => { 
  // TODO: All of this needs to be in a service or a class.
  const boldEscapeSequence = '\x1b[1m'
  const linkEscapeSequence = '\x1b[1;4;40;33m'
  const endEscapeSequence = '\x1b[0m'
  const wrapWithBoldStyle = string => `${boldEscapeSequence}${string}${endEscapeSequence}`
  const wrapWithLinkStyle = string => {
    const [, link, trailingWhitespace] = string.match(/^(.+?)(\s+?)$/) || []
    return `${linkEscapeSequence}${link || string}${endEscapeSequence}${trailingWhitespace || ''}`
  }
  const wrapWithHash = string => `# ${string} #`
  const prependNewLine = string => `\n${string}`
  const padWithSpaces = (string, lineLength) => string.padEnd(lineLength, ' ')
  
  const [, gitLabUserName, gitLabProjectPath] = stdout.match(/gitlab\.com[:\/](.+?)\/(.+?)\.git\s+\(fetch\)$/m) || []

  if (!gitLabUserName || stderr) {
    console.error(stderr || 'fatal: this repository does not have a GitLab remote.\n')
    process.exit(1)
  }

  const gitLabPageURL = `https://${gitLabUserName}.gitlab.io/${gitLabProjectPath}`
  const message = 'Once pushed and merged, your GitLab Page URL will be:'
  const lineLength = gitLabPageURL.length >= message.length ? gitLabPageURL.length : message.length

  const horizontalRule = prependNewLine('#'.repeat(lineLength + 4))
  const messageLine = prependNewLine(wrapWithHash(wrapWithBoldStyle(padWithSpaces(message, lineLength))))
  const urlLine = prependNewLine(wrapWithHash(wrapWithLinkStyle(padWithSpaces(gitLabPageURL, lineLength))))

  console.log(`\n${horizontalRule}${messageLine}${urlLine}${horizontalRule}\n`)

  const [, , path = process.cwd()] = process.argv
  const publicExists = fs.existsSync(path + '/public')
  const publicIsEmpty = publicExists && !(fs.readdirSync(path + '/public').length)
  
  if (publicExists && publicIsEmpty) {
    console.warn(
      `WARNING: This project's "public" directory is currently empty.\nPlease delete it or add something servable (such as an "index.html" file), to it.`
    )
  }
  
  const YAMLDoc = buildYAMLDoc(publicExists)
  
  console.log(
    `Writing the following configuration to "${path + '/.gitlab-ci.yml'}":\n${YAMLDoc}`
  )
  
  const writeStream = fs.createWriteStream(path + '/.gitlab-ci.yml')
  writeStream.write(YAMLDoc)
  writeStream.end()
})

